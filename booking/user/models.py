import uuid

from django.db import models
from django.utils import timezone


class Published(models.Model):
    id = models.CharField(primary_key=True, default='val1',max_length=15)
    seat = models.CharField(max_length=15)
    dateval = models.DateField(default=timezone.now())
