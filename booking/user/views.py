import uuid
from datetime import datetime

from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone

from user.models import Published


def save_entry(request):
    Published.objects.all().delete()
    key = request.GET["id"]
    seat_no = request.GET["seat"]
    new = Published(id=key, seat=seat_no,dateval=datetime.now())
    new.save()
    return HttpResponse('{response:"saved"}', status=200)
def retrive(request):
    if 'id' in request.GET and Published.objects.filter(id=request.GET['id']).exists():
        one_entry = Published.objects.get(pk=request.GET['id'])
        if one_entry:
            out=one_entry.dateval
        else:
            out = "no booking found"
    else:
        out="no user found"
    return HttpResponse(out, status=200)
