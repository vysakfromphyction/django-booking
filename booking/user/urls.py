from django.urls import path
from . import views

urlpatterns = [
    path('ticket/', views.save_entry, name='ticket'),
    path('viewticket/', views.retrive, name='viewticket'),
]
